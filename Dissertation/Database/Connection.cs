﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dissertation
{
    class Connection
    {
        public static string ConString(string nameConnection)
        {
            return ConfigurationManager.ConnectionStrings[nameConnection].ConnectionString;
        }
    }
}
