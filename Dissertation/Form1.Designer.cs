﻿
namespace Dissertation
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.DateAppointment = new System.Windows.Forms.DateTimePicker();
            this.patientNameLabel = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.AppointmentDoctorNameComboBox = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.AppointmentWardNameTextBox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.StartAppointmentdateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.nameLabel = new System.Windows.Forms.Label();
            this.namePatient = new System.Windows.Forms.TextBox();
            this.addressLabel = new System.Windows.Forms.Label();
            this.address = new System.Windows.Forms.TextBox();
            this.phone_numberLabel = new System.Windows.Forms.Label();
            this.phone_number = new System.Windows.Forms.TextBox();
            this.CNP_Label = new System.Windows.Forms.Label();
            this.CNP = new System.Windows.Forms.TextBox();
            this.insertPatient = new System.Windows.Forms.Button();
            this.patientDataGridView = new System.Windows.Forms.DataGridView();
            this.Column21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column22 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column23 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column24 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.patientBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.updatePatient = new System.Windows.Forms.Button();
            this.deletePatient = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.nameWard = new System.Windows.Forms.TextBox();
            this.nameWardLabel = new System.Windows.Forms.Label();
            this.wardDataGridView = new System.Windows.Forms.DataGridView();
            this.idwardDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nameDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.wardBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.wardDataSet = new Dissertation.wardDataSet();
            this.button1 = new System.Windows.Forms.Button();
            this.insertWard = new System.Windows.Forms.Button();
            this.deleteWard = new System.Windows.Forms.Button();
            this.updateWard = new System.Windows.Forms.Button();
            this.wardTableAdapter = new Dissertation.wardDataSetTableAdapters.WardTableAdapter();
            this.doctorTitleLabel = new System.Windows.Forms.Label();
            this.doctorLabel = new System.Windows.Forms.Label();
            this.doctorNameTextBox = new System.Windows.Forms.TextBox();
            this.doctorDataGridView = new System.Windows.Forms.DataGridView();
            this.iddoctorDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.nameDataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idwardDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.doctorBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.doctorDataSet = new Dissertation.DoctorDataSet();
            this.doctorTableAdapter = new Dissertation.DoctorDataSetTableAdapters.DoctorTableAdapter();
            this.insertDoctorButton = new System.Windows.Forms.Button();
            this.updateDoctorButton = new System.Windows.Forms.Button();
            this.deleteDoctorButton = new System.Windows.Forms.Button();
            this.serviceTitleLabel = new System.Windows.Forms.Label();
            this.serviceNameLabel = new System.Windows.Forms.Label();
            this.serviceNameTextBox = new System.Windows.Forms.TextBox();
            this.servicePriceTextBox = new System.Windows.Forms.TextBox();
            this.servicePriceLabel = new System.Windows.Forms.Label();
            this.servicesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.insertServicesButton = new System.Windows.Forms.Button();
            this.servicesUpdateButton = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.servicesDeleteButton = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.DoctorWardNameTextBox = new System.Windows.Forms.TextBox();
            this.endAppointmentdateTimePicker = new System.Windows.Forms.DateTimePicker();
            this.InsertAppointmentButton = new System.Windows.Forms.Button();
            this.UpdateAppointmentButton = new System.Windows.Forms.Button();
            this.DeleteAppointmentButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.serviceWardNameTextBox = new System.Windows.Forms.TextBox();
            this.consultationLabel = new System.Windows.Forms.Label();
            this.appointmentPatientNameComboBox = new System.Windows.Forms.ComboBox();
            this.AppointmentsDataGridView = new System.Windows.Forms.DataGridView();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.appointmentsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.ConsultationDateLabel = new System.Windows.Forms.Label();
            this.ConsultationPatientNameLabel = new System.Windows.Forms.Label();
            this.ConsultationDoctorNameLabel = new System.Windows.Forms.Label();
            this.ConsultationWardNameLabel = new System.Windows.Forms.Label();
            this.ConsultationServiceNameLabel = new System.Windows.Forms.Label();
            this.ConsultationPriceLabel = new System.Windows.Forms.Label();
            this.ConsultationDate = new System.Windows.Forms.DateTimePicker();
            this.ConsultationPatientNameComboBox = new System.Windows.Forms.ComboBox();
            this.ConsultationDoctorNameComboBox = new System.Windows.Forms.ComboBox();
            this.ConsultationWardNameTextBox = new System.Windows.Forms.TextBox();
            this.ConsultationServiceNameComboBox = new System.Windows.Forms.ComboBox();
            this.ConsultationPriceTextBox = new System.Windows.Forms.TextBox();
            this.ConsultationDataGridView = new System.Windows.Forms.DataGridView();
            this.Column12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.consultationsBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.consultationBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.label17 = new System.Windows.Forms.Label();
            this.ConsultationInsertButton = new System.Windows.Forms.Button();
            this.ConsultationUpdateButton = new System.Windows.Forms.Button();
            this.ConsultationDeleteButton = new System.Windows.Forms.Button();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.languagesBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.consultationsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.consultationBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.serviceDataGridView = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.serviceBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.label10 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.patientDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.patientBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wardDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wardBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wardDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.doctorDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.doctorBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.doctorDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.servicesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AppointmentsDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.appointmentsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ConsultationDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.consultationsBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.consultationBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.languagesBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.consultationsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.consultationBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.serviceDataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.serviceBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // DateAppointment
            // 
            resources.ApplyResources(this.DateAppointment, "DateAppointment");
            this.DateAppointment.Name = "DateAppointment";
            // 
            // patientNameLabel
            // 
            resources.ApplyResources(this.patientNameLabel, "patientNameLabel");
            this.patientNameLabel.Name = "patientNameLabel";
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // AppointmentDoctorNameComboBox
            // 
            this.AppointmentDoctorNameComboBox.AllowDrop = true;
            this.AppointmentDoctorNameComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.AppointmentDoctorNameComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.AppointmentDoctorNameComboBox.FormattingEnabled = true;
            resources.ApplyResources(this.AppointmentDoctorNameComboBox, "AppointmentDoctorNameComboBox");
            this.AppointmentDoctorNameComboBox.Name = "AppointmentDoctorNameComboBox";
            this.AppointmentDoctorNameComboBox.TextChanged += new System.EventHandler(this.AppointmentDoctorNameComboBox_TextChanged);
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            // 
            // AppointmentWardNameTextBox
            // 
            resources.ApplyResources(this.AppointmentWardNameTextBox, "AppointmentWardNameTextBox");
            this.AppointmentWardNameTextBox.Name = "AppointmentWardNameTextBox";
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.Name = "label4";
            // 
            // StartAppointmentdateTimePicker
            // 
            this.StartAppointmentdateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            resources.ApplyResources(this.StartAppointmentdateTimePicker, "StartAppointmentdateTimePicker");
            this.StartAppointmentdateTimePicker.Name = "StartAppointmentdateTimePicker";
            // 
            // label5
            // 
            resources.ApplyResources(this.label5, "label5");
            this.label5.Name = "label5";
            // 
            // label6
            // 
            resources.ApplyResources(this.label6, "label6");
            this.label6.Name = "label6";
            // 
            // menuStrip1
            // 
            resources.ApplyResources(this.menuStrip1, "menuStrip1");
            this.menuStrip1.Name = "menuStrip1";
            // 
            // nameLabel
            // 
            resources.ApplyResources(this.nameLabel, "nameLabel");
            this.nameLabel.Name = "nameLabel";
            // 
            // namePatient
            // 
            resources.ApplyResources(this.namePatient, "namePatient");
            this.namePatient.Name = "namePatient";
            // 
            // addressLabel
            // 
            resources.ApplyResources(this.addressLabel, "addressLabel");
            this.addressLabel.Name = "addressLabel";
            // 
            // address
            // 
            resources.ApplyResources(this.address, "address");
            this.address.Name = "address";
            // 
            // phone_numberLabel
            // 
            resources.ApplyResources(this.phone_numberLabel, "phone_numberLabel");
            this.phone_numberLabel.Name = "phone_numberLabel";
            // 
            // phone_number
            // 
            resources.ApplyResources(this.phone_number, "phone_number");
            this.phone_number.Name = "phone_number";
            // 
            // CNP_Label
            // 
            resources.ApplyResources(this.CNP_Label, "CNP_Label");
            this.CNP_Label.Name = "CNP_Label";
            // 
            // CNP
            // 
            resources.ApplyResources(this.CNP, "CNP");
            this.CNP.Name = "CNP";
            // 
            // insertPatient
            // 
            resources.ApplyResources(this.insertPatient, "insertPatient");
            this.insertPatient.Name = "insertPatient";
            this.insertPatient.UseVisualStyleBackColor = true;
            this.insertPatient.Click += new System.EventHandler(this.insertPatient_Click);
            // 
            // patientDataGridView
            // 
            this.patientDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.patientDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column21,
            this.Column22,
            this.Column23,
            this.Column24,
            this.Column25});
            resources.ApplyResources(this.patientDataGridView, "patientDataGridView");
            this.patientDataGridView.Name = "patientDataGridView";
            this.patientDataGridView.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.patientDataGridView_CellEnter);
            // 
            // Column21
            // 
            resources.ApplyResources(this.Column21, "Column21");
            this.Column21.Name = "Column21";
            // 
            // Column22
            // 
            resources.ApplyResources(this.Column22, "Column22");
            this.Column22.Name = "Column22";
            // 
            // Column23
            // 
            resources.ApplyResources(this.Column23, "Column23");
            this.Column23.Name = "Column23";
            // 
            // Column24
            // 
            resources.ApplyResources(this.Column24, "Column24");
            this.Column24.Name = "Column24";
            // 
            // Column25
            // 
            resources.ApplyResources(this.Column25, "Column25");
            this.Column25.Name = "Column25";
            // 
            // patientBindingSource
            // 
            this.patientBindingSource.DataMember = "Patient";
            // 
            // updatePatient
            // 
            resources.ApplyResources(this.updatePatient, "updatePatient");
            this.updatePatient.Name = "updatePatient";
            this.updatePatient.UseVisualStyleBackColor = true;
            this.updatePatient.Click += new System.EventHandler(this.updatePatient_Click);
            // 
            // deletePatient
            // 
            resources.ApplyResources(this.deletePatient, "deletePatient");
            this.deletePatient.Name = "deletePatient";
            this.deletePatient.UseVisualStyleBackColor = true;
            this.deletePatient.Click += new System.EventHandler(this.deletePatient_Click);
            // 
            // label7
            // 
            resources.ApplyResources(this.label7, "label7");
            this.label7.Name = "label7";
            // 
            // label8
            // 
            resources.ApplyResources(this.label8, "label8");
            this.label8.Name = "label8";
            // 
            // nameWard
            // 
            resources.ApplyResources(this.nameWard, "nameWard");
            this.nameWard.Name = "nameWard";
            // 
            // nameWardLabel
            // 
            resources.ApplyResources(this.nameWardLabel, "nameWardLabel");
            this.nameWardLabel.Name = "nameWardLabel";
            // 
            // wardDataGridView
            // 
            this.wardDataGridView.AutoGenerateColumns = false;
            this.wardDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.wardDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.idwardDataGridViewTextBoxColumn,
            this.nameDataGridViewTextBoxColumn1});
            this.wardDataGridView.DataSource = this.wardBindingSource;
            resources.ApplyResources(this.wardDataGridView, "wardDataGridView");
            this.wardDataGridView.Name = "wardDataGridView";
            this.wardDataGridView.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.wardDataGridView_CellEnter);
            // 
            // idwardDataGridViewTextBoxColumn
            // 
            this.idwardDataGridViewTextBoxColumn.DataPropertyName = "id_ward";
            resources.ApplyResources(this.idwardDataGridViewTextBoxColumn, "idwardDataGridViewTextBoxColumn");
            this.idwardDataGridViewTextBoxColumn.Name = "idwardDataGridViewTextBoxColumn";
            this.idwardDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // nameDataGridViewTextBoxColumn1
            // 
            this.nameDataGridViewTextBoxColumn1.DataPropertyName = "name";
            resources.ApplyResources(this.nameDataGridViewTextBoxColumn1, "nameDataGridViewTextBoxColumn1");
            this.nameDataGridViewTextBoxColumn1.Name = "nameDataGridViewTextBoxColumn1";
            // 
            // wardBindingSource
            // 
            this.wardBindingSource.DataMember = "Ward";
            this.wardBindingSource.DataSource = this.wardDataSet;
            // 
            // wardDataSet
            // 
            this.wardDataSet.DataSetName = "wardDataSet";
            this.wardDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // button1
            // 
            resources.ApplyResources(this.button1, "button1");
            this.button1.Name = "button1";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // insertWard
            // 
            resources.ApplyResources(this.insertWard, "insertWard");
            this.insertWard.Name = "insertWard";
            this.insertWard.UseVisualStyleBackColor = true;
            this.insertWard.Click += new System.EventHandler(this.insertWard_Click);
            // 
            // deleteWard
            // 
            resources.ApplyResources(this.deleteWard, "deleteWard");
            this.deleteWard.Name = "deleteWard";
            this.deleteWard.UseVisualStyleBackColor = true;
            this.deleteWard.Click += new System.EventHandler(this.deleteWard_Click);
            // 
            // updateWard
            // 
            resources.ApplyResources(this.updateWard, "updateWard");
            this.updateWard.Name = "updateWard";
            this.updateWard.UseVisualStyleBackColor = true;
            this.updateWard.Click += new System.EventHandler(this.updateWard_Click);
            // 
            // wardTableAdapter
            // 
            this.wardTableAdapter.ClearBeforeFill = true;
            // 
            // doctorTitleLabel
            // 
            resources.ApplyResources(this.doctorTitleLabel, "doctorTitleLabel");
            this.doctorTitleLabel.Name = "doctorTitleLabel";
            // 
            // doctorLabel
            // 
            resources.ApplyResources(this.doctorLabel, "doctorLabel");
            this.doctorLabel.Name = "doctorLabel";
            // 
            // doctorNameTextBox
            // 
            resources.ApplyResources(this.doctorNameTextBox, "doctorNameTextBox");
            this.doctorNameTextBox.Name = "doctorNameTextBox";
            // 
            // doctorDataGridView
            // 
            this.doctorDataGridView.AutoGenerateColumns = false;
            this.doctorDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.doctorDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.iddoctorDataGridViewTextBoxColumn,
            this.nameDataGridViewTextBoxColumn2,
            this.idwardDataGridViewTextBoxColumn1});
            this.doctorDataGridView.DataSource = this.doctorBindingSource;
            resources.ApplyResources(this.doctorDataGridView, "doctorDataGridView");
            this.doctorDataGridView.Name = "doctorDataGridView";
            this.doctorDataGridView.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.doctorDataGridView_CellEnter);
            // 
            // iddoctorDataGridViewTextBoxColumn
            // 
            this.iddoctorDataGridViewTextBoxColumn.DataPropertyName = "id_doctor";
            resources.ApplyResources(this.iddoctorDataGridViewTextBoxColumn, "iddoctorDataGridViewTextBoxColumn");
            this.iddoctorDataGridViewTextBoxColumn.Name = "iddoctorDataGridViewTextBoxColumn";
            this.iddoctorDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // nameDataGridViewTextBoxColumn2
            // 
            this.nameDataGridViewTextBoxColumn2.DataPropertyName = "name";
            resources.ApplyResources(this.nameDataGridViewTextBoxColumn2, "nameDataGridViewTextBoxColumn2");
            this.nameDataGridViewTextBoxColumn2.Name = "nameDataGridViewTextBoxColumn2";
            // 
            // idwardDataGridViewTextBoxColumn1
            // 
            this.idwardDataGridViewTextBoxColumn1.DataPropertyName = "id_ward";
            resources.ApplyResources(this.idwardDataGridViewTextBoxColumn1, "idwardDataGridViewTextBoxColumn1");
            this.idwardDataGridViewTextBoxColumn1.Name = "idwardDataGridViewTextBoxColumn1";
            // 
            // doctorBindingSource
            // 
            this.doctorBindingSource.DataMember = "Doctor";
            this.doctorBindingSource.DataSource = this.doctorDataSet;
            // 
            // doctorDataSet
            // 
            this.doctorDataSet.DataSetName = "DoctorDataSet";
            this.doctorDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // doctorTableAdapter
            // 
            this.doctorTableAdapter.ClearBeforeFill = true;
            // 
            // insertDoctorButton
            // 
            resources.ApplyResources(this.insertDoctorButton, "insertDoctorButton");
            this.insertDoctorButton.Name = "insertDoctorButton";
            this.insertDoctorButton.UseVisualStyleBackColor = true;
            this.insertDoctorButton.Click += new System.EventHandler(this.insertDoctorButton_Click);
            // 
            // updateDoctorButton
            // 
            resources.ApplyResources(this.updateDoctorButton, "updateDoctorButton");
            this.updateDoctorButton.Name = "updateDoctorButton";
            this.updateDoctorButton.UseVisualStyleBackColor = true;
            this.updateDoctorButton.Click += new System.EventHandler(this.updateDoctorButton_Click);
            // 
            // deleteDoctorButton
            // 
            resources.ApplyResources(this.deleteDoctorButton, "deleteDoctorButton");
            this.deleteDoctorButton.Name = "deleteDoctorButton";
            this.deleteDoctorButton.UseVisualStyleBackColor = true;
            this.deleteDoctorButton.Click += new System.EventHandler(this.deleteDoctorButton_Click);
            // 
            // serviceTitleLabel
            // 
            resources.ApplyResources(this.serviceTitleLabel, "serviceTitleLabel");
            this.serviceTitleLabel.Name = "serviceTitleLabel";
            // 
            // serviceNameLabel
            // 
            resources.ApplyResources(this.serviceNameLabel, "serviceNameLabel");
            this.serviceNameLabel.Name = "serviceNameLabel";
            // 
            // serviceNameTextBox
            // 
            resources.ApplyResources(this.serviceNameTextBox, "serviceNameTextBox");
            this.serviceNameTextBox.Name = "serviceNameTextBox";
            // 
            // servicePriceTextBox
            // 
            resources.ApplyResources(this.servicePriceTextBox, "servicePriceTextBox");
            this.servicePriceTextBox.Name = "servicePriceTextBox";
            // 
            // servicePriceLabel
            // 
            resources.ApplyResources(this.servicePriceLabel, "servicePriceLabel");
            this.servicePriceLabel.Name = "servicePriceLabel";
            // 
            // servicesBindingSource
            // 
            this.servicesBindingSource.DataMember = "Services";
            // 
            // insertServicesButton
            // 
            resources.ApplyResources(this.insertServicesButton, "insertServicesButton");
            this.insertServicesButton.Name = "insertServicesButton";
            this.insertServicesButton.UseVisualStyleBackColor = true;
            this.insertServicesButton.Click += new System.EventHandler(this.insertServicesButton_Click);
            // 
            // servicesUpdateButton
            // 
            resources.ApplyResources(this.servicesUpdateButton, "servicesUpdateButton");
            this.servicesUpdateButton.Name = "servicesUpdateButton";
            this.servicesUpdateButton.UseVisualStyleBackColor = true;
            this.servicesUpdateButton.Click += new System.EventHandler(this.servicesUpdateButton_Click);
            // 
            // button4
            // 
            resources.ApplyResources(this.button4, "button4");
            this.button4.Name = "button4";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // servicesDeleteButton
            // 
            resources.ApplyResources(this.servicesDeleteButton, "servicesDeleteButton");
            this.servicesDeleteButton.Name = "servicesDeleteButton";
            this.servicesDeleteButton.UseVisualStyleBackColor = true;
            this.servicesDeleteButton.Click += new System.EventHandler(this.servicesDeleteButton_Click);
            // 
            // label9
            // 
            resources.ApplyResources(this.label9, "label9");
            this.label9.Name = "label9";
            // 
            // DoctorWardNameTextBox
            // 
            resources.ApplyResources(this.DoctorWardNameTextBox, "DoctorWardNameTextBox");
            this.DoctorWardNameTextBox.Name = "DoctorWardNameTextBox";
            // 
            // endAppointmentdateTimePicker
            // 
            this.endAppointmentdateTimePicker.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            resources.ApplyResources(this.endAppointmentdateTimePicker, "endAppointmentdateTimePicker");
            this.endAppointmentdateTimePicker.Name = "endAppointmentdateTimePicker";
            // 
            // InsertAppointmentButton
            // 
            resources.ApplyResources(this.InsertAppointmentButton, "InsertAppointmentButton");
            this.InsertAppointmentButton.Name = "InsertAppointmentButton";
            this.InsertAppointmentButton.UseVisualStyleBackColor = true;
            this.InsertAppointmentButton.Click += new System.EventHandler(this.InsertAppointmentButton_Click);
            // 
            // UpdateAppointmentButton
            // 
            resources.ApplyResources(this.UpdateAppointmentButton, "UpdateAppointmentButton");
            this.UpdateAppointmentButton.Name = "UpdateAppointmentButton";
            this.UpdateAppointmentButton.UseVisualStyleBackColor = true;
            this.UpdateAppointmentButton.Click += new System.EventHandler(this.UpdateAppointmentButton_Click);
            // 
            // DeleteAppointmentButton
            // 
            resources.ApplyResources(this.DeleteAppointmentButton, "DeleteAppointmentButton");
            this.DeleteAppointmentButton.Name = "DeleteAppointmentButton";
            this.DeleteAppointmentButton.UseVisualStyleBackColor = true;
            this.DeleteAppointmentButton.Click += new System.EventHandler(this.DeleteAppointmentButton_Click);
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // serviceWardNameTextBox
            // 
            resources.ApplyResources(this.serviceWardNameTextBox, "serviceWardNameTextBox");
            this.serviceWardNameTextBox.Name = "serviceWardNameTextBox";
            // 
            // consultationLabel
            // 
            resources.ApplyResources(this.consultationLabel, "consultationLabel");
            this.consultationLabel.Name = "consultationLabel";
            // 
            // appointmentPatientNameComboBox
            // 
            this.appointmentPatientNameComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.appointmentPatientNameComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.appointmentPatientNameComboBox.FormattingEnabled = true;
            resources.ApplyResources(this.appointmentPatientNameComboBox, "appointmentPatientNameComboBox");
            this.appointmentPatientNameComboBox.Name = "appointmentPatientNameComboBox";
            // 
            // AppointmentsDataGridView
            // 
            this.AppointmentsDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.AppointmentsDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column5,
            this.Column6,
            this.Column7,
            this.Column8,
            this.Column9,
            this.Column10,
            this.Column11});
            resources.ApplyResources(this.AppointmentsDataGridView, "AppointmentsDataGridView");
            this.AppointmentsDataGridView.Name = "AppointmentsDataGridView";
            this.AppointmentsDataGridView.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.AppointmentsDataGridView_CellEnter);
            // 
            // Column5
            // 
            resources.ApplyResources(this.Column5, "Column5");
            this.Column5.Name = "Column5";
            // 
            // Column6
            // 
            resources.ApplyResources(this.Column6, "Column6");
            this.Column6.Name = "Column6";
            // 
            // Column7
            // 
            resources.ApplyResources(this.Column7, "Column7");
            this.Column7.Name = "Column7";
            // 
            // Column8
            // 
            resources.ApplyResources(this.Column8, "Column8");
            this.Column8.Name = "Column8";
            // 
            // Column9
            // 
            resources.ApplyResources(this.Column9, "Column9");
            this.Column9.Name = "Column9";
            // 
            // Column10
            // 
            resources.ApplyResources(this.Column10, "Column10");
            this.Column10.Name = "Column10";
            // 
            // Column11
            // 
            resources.ApplyResources(this.Column11, "Column11");
            this.Column11.Name = "Column11";
            // 
            // appointmentsBindingSource
            // 
            this.appointmentsBindingSource.DataMember = "Appointments";
            // 
            // ConsultationDateLabel
            // 
            resources.ApplyResources(this.ConsultationDateLabel, "ConsultationDateLabel");
            this.ConsultationDateLabel.Name = "ConsultationDateLabel";
            // 
            // ConsultationPatientNameLabel
            // 
            resources.ApplyResources(this.ConsultationPatientNameLabel, "ConsultationPatientNameLabel");
            this.ConsultationPatientNameLabel.Name = "ConsultationPatientNameLabel";
            // 
            // ConsultationDoctorNameLabel
            // 
            resources.ApplyResources(this.ConsultationDoctorNameLabel, "ConsultationDoctorNameLabel");
            this.ConsultationDoctorNameLabel.Name = "ConsultationDoctorNameLabel";
            // 
            // ConsultationWardNameLabel
            // 
            resources.ApplyResources(this.ConsultationWardNameLabel, "ConsultationWardNameLabel");
            this.ConsultationWardNameLabel.Name = "ConsultationWardNameLabel";
            // 
            // ConsultationServiceNameLabel
            // 
            resources.ApplyResources(this.ConsultationServiceNameLabel, "ConsultationServiceNameLabel");
            this.ConsultationServiceNameLabel.Name = "ConsultationServiceNameLabel";
            // 
            // ConsultationPriceLabel
            // 
            resources.ApplyResources(this.ConsultationPriceLabel, "ConsultationPriceLabel");
            this.ConsultationPriceLabel.Name = "ConsultationPriceLabel";
            // 
            // ConsultationDate
            // 
            resources.ApplyResources(this.ConsultationDate, "ConsultationDate");
            this.ConsultationDate.Name = "ConsultationDate";
            // 
            // ConsultationPatientNameComboBox
            // 
            this.ConsultationPatientNameComboBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.ConsultationPatientNameComboBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.ConsultationPatientNameComboBox.FormattingEnabled = true;
            resources.ApplyResources(this.ConsultationPatientNameComboBox, "ConsultationPatientNameComboBox");
            this.ConsultationPatientNameComboBox.Name = "ConsultationPatientNameComboBox";
            // 
            // ConsultationDoctorNameComboBox
            // 
            this.ConsultationDoctorNameComboBox.FormattingEnabled = true;
            resources.ApplyResources(this.ConsultationDoctorNameComboBox, "ConsultationDoctorNameComboBox");
            this.ConsultationDoctorNameComboBox.Name = "ConsultationDoctorNameComboBox";
            this.ConsultationDoctorNameComboBox.TextChanged += new System.EventHandler(this.ConsultationDoctorNameComboBox_TextChanged);
            // 
            // ConsultationWardNameTextBox
            // 
            resources.ApplyResources(this.ConsultationWardNameTextBox, "ConsultationWardNameTextBox");
            this.ConsultationWardNameTextBox.Name = "ConsultationWardNameTextBox";
            this.ConsultationWardNameTextBox.TextChanged += new System.EventHandler(this.ConsultationWardNameTextBox_TextChanged);
            // 
            // ConsultationServiceNameComboBox
            // 
            this.ConsultationServiceNameComboBox.FormattingEnabled = true;
            resources.ApplyResources(this.ConsultationServiceNameComboBox, "ConsultationServiceNameComboBox");
            this.ConsultationServiceNameComboBox.Name = "ConsultationServiceNameComboBox";
            this.ConsultationServiceNameComboBox.TextChanged += new System.EventHandler(this.ConsultationServiceNameComboBox_TextChanged);
            // 
            // ConsultationPriceTextBox
            // 
            resources.ApplyResources(this.ConsultationPriceTextBox, "ConsultationPriceTextBox");
            this.ConsultationPriceTextBox.Name = "ConsultationPriceTextBox";
            // 
            // ConsultationDataGridView
            // 
            this.ConsultationDataGridView.AllowUserToOrderColumns = true;
            this.ConsultationDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ConsultationDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column12,
            this.Column13,
            this.Column14,
            this.Column15,
            this.Column16,
            this.Column17,
            this.Column18,
            this.Column19,
            this.Column20});
            resources.ApplyResources(this.ConsultationDataGridView, "ConsultationDataGridView");
            this.ConsultationDataGridView.Name = "ConsultationDataGridView";
            this.ConsultationDataGridView.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.ConsultationDataGridView_CellEnter);
            // 
            // Column12
            // 
            resources.ApplyResources(this.Column12, "Column12");
            this.Column12.Name = "Column12";
            // 
            // Column13
            // 
            resources.ApplyResources(this.Column13, "Column13");
            this.Column13.Name = "Column13";
            // 
            // Column14
            // 
            resources.ApplyResources(this.Column14, "Column14");
            this.Column14.Name = "Column14";
            // 
            // Column15
            // 
            resources.ApplyResources(this.Column15, "Column15");
            this.Column15.Name = "Column15";
            // 
            // Column16
            // 
            resources.ApplyResources(this.Column16, "Column16");
            this.Column16.Name = "Column16";
            // 
            // Column17
            // 
            resources.ApplyResources(this.Column17, "Column17");
            this.Column17.Name = "Column17";
            // 
            // Column18
            // 
            resources.ApplyResources(this.Column18, "Column18");
            this.Column18.Name = "Column18";
            // 
            // Column19
            // 
            resources.ApplyResources(this.Column19, "Column19");
            this.Column19.Name = "Column19";
            // 
            // Column20
            // 
            resources.ApplyResources(this.Column20, "Column20");
            this.Column20.Name = "Column20";
            // 
            // consultationsBindingSource1
            // 
            this.consultationsBindingSource1.DataMember = "Consultations";
            // 
            // label17
            // 
            resources.ApplyResources(this.label17, "label17");
            this.label17.Name = "label17";
            // 
            // ConsultationInsertButton
            // 
            resources.ApplyResources(this.ConsultationInsertButton, "ConsultationInsertButton");
            this.ConsultationInsertButton.Name = "ConsultationInsertButton";
            this.ConsultationInsertButton.UseVisualStyleBackColor = true;
            this.ConsultationInsertButton.Click += new System.EventHandler(this.ConsultationInsertButton_Click);
            // 
            // ConsultationUpdateButton
            // 
            resources.ApplyResources(this.ConsultationUpdateButton, "ConsultationUpdateButton");
            this.ConsultationUpdateButton.Name = "ConsultationUpdateButton";
            this.ConsultationUpdateButton.UseVisualStyleBackColor = true;
            this.ConsultationUpdateButton.Click += new System.EventHandler(this.ConsultationUpdateButton_Click);
            // 
            // ConsultationDeleteButton
            // 
            resources.ApplyResources(this.ConsultationDeleteButton, "ConsultationDeleteButton");
            this.ConsultationDeleteButton.Name = "ConsultationDeleteButton";
            this.ConsultationDeleteButton.UseVisualStyleBackColor = true;
            this.ConsultationDeleteButton.Click += new System.EventHandler(this.ConsultationDeleteButton_Click);
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            resources.GetString("comboBox1.Items"),
            resources.GetString("comboBox1.Items1")});
            resources.ApplyResources(this.comboBox1, "comboBox1");
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.TextChanged += new System.EventHandler(this.comboBox1_TextChanged);
            // 
            // languagesBindingSource
            // 
            this.languagesBindingSource.DataMember = "Languages";
            // 
            // consultationsBindingSource
            // 
            this.consultationsBindingSource.DataMember = "Consultations";
            // 
            // consultationBindingSource
            // 
            this.consultationBindingSource.DataMember = "Consultation";
            // 
            // serviceDataGridView
            // 
            this.serviceDataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.serviceDataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4});
            resources.ApplyResources(this.serviceDataGridView, "serviceDataGridView");
            this.serviceDataGridView.Name = "serviceDataGridView";
            this.serviceDataGridView.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.serviceDataGridView_CellEnter_1);
            // 
            // Column1
            // 
            resources.ApplyResources(this.Column1, "Column1");
            this.Column1.Name = "Column1";
            // 
            // Column2
            // 
            resources.ApplyResources(this.Column2, "Column2");
            this.Column2.Name = "Column2";
            // 
            // Column3
            // 
            resources.ApplyResources(this.Column3, "Column3");
            this.Column3.Name = "Column3";
            // 
            // Column4
            // 
            resources.ApplyResources(this.Column4, "Column4");
            this.Column4.Name = "Column4";
            // 
            // serviceBindingSource
            // 
            this.serviceBindingSource.DataMember = "Service";
            // 
            // label10
            // 
            resources.ApplyResources(this.label10, "label10");
            this.label10.Name = "label10";
            // 
            // textBox1
            // 
            resources.ApplyResources(this.textBox1, "textBox1");
            this.textBox1.Name = "textBox1";
            // 
            // Form1
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.serviceDataGridView);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.ConsultationDeleteButton);
            this.Controls.Add(this.ConsultationUpdateButton);
            this.Controls.Add(this.ConsultationInsertButton);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.ConsultationDataGridView);
            this.Controls.Add(this.ConsultationPriceTextBox);
            this.Controls.Add(this.ConsultationServiceNameComboBox);
            this.Controls.Add(this.ConsultationWardNameTextBox);
            this.Controls.Add(this.ConsultationDoctorNameComboBox);
            this.Controls.Add(this.ConsultationPatientNameComboBox);
            this.Controls.Add(this.ConsultationDate);
            this.Controls.Add(this.ConsultationWardNameLabel);
            this.Controls.Add(this.ConsultationDoctorNameLabel);
            this.Controls.Add(this.ConsultationPatientNameLabel);
            this.Controls.Add(this.ConsultationPriceLabel);
            this.Controls.Add(this.ConsultationServiceNameLabel);
            this.Controls.Add(this.ConsultationDateLabel);
            this.Controls.Add(this.AppointmentsDataGridView);
            this.Controls.Add(this.appointmentPatientNameComboBox);
            this.Controls.Add(this.consultationLabel);
            this.Controls.Add(this.serviceWardNameTextBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.DeleteAppointmentButton);
            this.Controls.Add(this.UpdateAppointmentButton);
            this.Controls.Add(this.InsertAppointmentButton);
            this.Controls.Add(this.DoctorWardNameTextBox);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.servicesDeleteButton);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.servicesUpdateButton);
            this.Controls.Add(this.insertServicesButton);
            this.Controls.Add(this.servicePriceLabel);
            this.Controls.Add(this.servicePriceTextBox);
            this.Controls.Add(this.serviceNameTextBox);
            this.Controls.Add(this.serviceNameLabel);
            this.Controls.Add(this.serviceTitleLabel);
            this.Controls.Add(this.deleteDoctorButton);
            this.Controls.Add(this.updateDoctorButton);
            this.Controls.Add(this.insertDoctorButton);
            this.Controls.Add(this.doctorDataGridView);
            this.Controls.Add(this.doctorNameTextBox);
            this.Controls.Add(this.doctorLabel);
            this.Controls.Add(this.doctorTitleLabel);
            this.Controls.Add(this.updateWard);
            this.Controls.Add(this.deleteWard);
            this.Controls.Add(this.insertWard);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.wardDataGridView);
            this.Controls.Add(this.nameWardLabel);
            this.Controls.Add(this.nameWard);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.deletePatient);
            this.Controls.Add(this.updatePatient);
            this.Controls.Add(this.patientDataGridView);
            this.Controls.Add(this.insertPatient);
            this.Controls.Add(this.CNP);
            this.Controls.Add(this.CNP_Label);
            this.Controls.Add(this.phone_number);
            this.Controls.Add(this.phone_numberLabel);
            this.Controls.Add(this.address);
            this.Controls.Add(this.addressLabel);
            this.Controls.Add(this.namePatient);
            this.Controls.Add(this.nameLabel);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.endAppointmentdateTimePicker);
            this.Controls.Add(this.StartAppointmentdateTimePicker);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.AppointmentWardNameTextBox);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.AppointmentDoctorNameComboBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.patientNameLabel);
            this.Controls.Add(this.DateAppointment);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.patientDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.patientBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wardDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wardBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wardDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.doctorDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.doctorBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.doctorDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.servicesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AppointmentsDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.appointmentsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ConsultationDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.consultationsBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.consultationBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.languagesBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.consultationsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.consultationBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.serviceDataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.serviceBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DateTimePicker DateAppointment;
        private System.Windows.Forms.Label patientNameLabel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox AppointmentDoctorNameComboBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox AppointmentWardNameTextBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker StartAppointmentdateTimePicker;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.Label addressLabel;
        private System.Windows.Forms.TextBox address;
        private System.Windows.Forms.Label phone_numberLabel;
        private System.Windows.Forms.TextBox phone_number;
        private System.Windows.Forms.Label CNP_Label;
        private System.Windows.Forms.TextBox CNP;
        public System.Windows.Forms.TextBox namePatient;
        private System.Windows.Forms.Button insertPatient;
        private System.Windows.Forms.DataGridView patientDataGridView;
        private System.Windows.Forms.BindingSource patientBindingSource;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox nameWard;
        private System.Windows.Forms.Label nameWardLabel;
        private System.Windows.Forms.DataGridView wardDataGridView;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button insertWard;
        private System.Windows.Forms.Button deleteWard;
        private System.Windows.Forms.Button updateWard;
        private wardDataSet wardDataSet;
        private System.Windows.Forms.BindingSource wardBindingSource;
        private wardDataSetTableAdapters.WardTableAdapter wardTableAdapter;
        private System.Windows.Forms.Label doctorTitleLabel;
        private System.Windows.Forms.Label doctorLabel;
        private System.Windows.Forms.TextBox doctorNameTextBox;
        private System.Windows.Forms.DataGridView doctorDataGridView;
        private DoctorDataSet doctorDataSet;
        private System.Windows.Forms.BindingSource doctorBindingSource;
        private DoctorDataSetTableAdapters.DoctorTableAdapter doctorTableAdapter;
        private System.Windows.Forms.Button insertDoctorButton;
        private System.Windows.Forms.Button updateDoctorButton;
        private System.Windows.Forms.Button deleteDoctorButton;
        private System.Windows.Forms.Label serviceTitleLabel;
        private System.Windows.Forms.Label serviceNameLabel;
        private System.Windows.Forms.TextBox serviceNameTextBox;
        private System.Windows.Forms.TextBox servicePriceTextBox;
        private System.Windows.Forms.Label servicePriceLabel;
        private System.Windows.Forms.Button insertServicesButton;
        private System.Windows.Forms.Button servicesUpdateButton;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button servicesDeleteButton;
        
        private System.Windows.Forms.BindingSource servicesBindingSource;
      
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox DoctorWardNameTextBox;
        private System.Windows.Forms.DateTimePicker endAppointmentdateTimePicker;
        private System.Windows.Forms.Button InsertAppointmentButton;
        private System.Windows.Forms.Button UpdateAppointmentButton;
        private System.Windows.Forms.Button DeleteAppointmentButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox serviceWardNameTextBox;
       // private ConsultationDataSet consultationDataSet;
        private System.Windows.Forms.BindingSource consultationBindingSource;
       // private ConsultationDataSetTableAdapters.ConsultationTableAdapter consultationTableAdapter;
        
        private System.Windows.Forms.Label consultationLabel;
        private System.Windows.Forms.ComboBox appointmentPatientNameComboBox;
       
        private System.Windows.Forms.BindingSource consultationsBindingSource;
        private System.Windows.Forms.DataGridView AppointmentsDataGridView;
        
        private System.Windows.Forms.BindingSource appointmentsBindingSource;
        
        private System.Windows.Forms.Label ConsultationDateLabel;
        private System.Windows.Forms.Label ConsultationPatientNameLabel;
        private System.Windows.Forms.Label ConsultationDoctorNameLabel;
        private System.Windows.Forms.Label ConsultationWardNameLabel;
        private System.Windows.Forms.Label ConsultationServiceNameLabel;
        private System.Windows.Forms.Label ConsultationPriceLabel;
        private System.Windows.Forms.DateTimePicker ConsultationDate;
        private System.Windows.Forms.ComboBox ConsultationPatientNameComboBox;
        private System.Windows.Forms.ComboBox ConsultationDoctorNameComboBox;
        private System.Windows.Forms.TextBox ConsultationWardNameTextBox;
        private System.Windows.Forms.ComboBox ConsultationServiceNameComboBox;
        private System.Windows.Forms.TextBox ConsultationPriceTextBox;
        private System.Windows.Forms.DataGridView ConsultationDataGridView;
       
        private System.Windows.Forms.BindingSource consultationBindingSource1;
       
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button ConsultationInsertButton;
        private System.Windows.Forms.Button ConsultationUpdateButton;
        private System.Windows.Forms.Button ConsultationDeleteButton;
        private System.Windows.Forms.DataGridViewTextBoxColumn iDDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn englishDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn romanianDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource languagesBindingSource;
       
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.DataGridViewTextBoxColumn idconsultationDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn patientnameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn idpatientDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn iddoctorDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn idwardDataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn servicenameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn idserviceDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn priceDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dateDataGridViewTextBoxColumn;
       
       
        private System.Windows.Forms.BindingSource consultationsBindingSource1;
        
        public System.Windows.Forms.Button updatePatient;
        public System.Windows.Forms.Button deletePatient;
        public System.Windows.Forms.Label nameLabel;
        private System.Windows.Forms.DataGridView serviceDataGridView;
       
        private System.Windows.Forms.BindingSource serviceBindingSource;
       
        private System.Windows.Forms.DataGridViewTextBoxColumn idserviceDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn priceDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn idwardDataGridViewTextBoxColumn2;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.DataGridViewTextBoxColumn idwardDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn iddoctorDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn idwardDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn idpatientDataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn iddoctorDataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn idwardDataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn idserviceDataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn priceDataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column12;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column13;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column14;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column15;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column16;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column17;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column18;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column19;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column20;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column21;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column22;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column23;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column24;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column25;
      
    }
}

