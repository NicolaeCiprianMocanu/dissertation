﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Resources;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Dapper;
using Dissertation.CRUD_Operations;
using Dissertation.DataTypes;

namespace Dissertation
{
    public partial class Form1 : Form
    {
        public static int patientID,wardID;
        private int doctorID;
        private int serviceID;
        private int consultationID;
        private int appointmentID;
        public bool boolean;
        private string language;
        
      
       
        public Form1()
        {
            InitializeComponent();
            loadComboBoxPatientNames();
            loadComboBoxDoctorNames();
            resetMe();
        }

        private void loadServicesList()
        {
            System.Data.SqlClient.SqlConnection connection = new System.Data.SqlClient.SqlConnection
               (Connection.ConString("ClinicDB"));


            connection.Open();
            //query to get all PATIENTs name 
            SqlCommand cmd = new SqlCommand("Select * FROM Service", connection);

            SqlDataReader da = cmd.ExecuteReader();
            int rows = 0;
            if (da.HasRows)
            {
                decimal price;
                while (da.Read())
                {
                   
                  
                    if (language == "english")
                    {
                        // Create a CultureInfo object for French in France.
                        CultureInfo enCulture = new CultureInfo("en-US");
                        // Set the CurrentCulture to fr-FR.
                        Thread.CurrentThread.CurrentCulture = enCulture;
                        // Clone the NumberFormatInfo object and create
                        // a new object for the local currency of France.
                        NumberFormatInfo LocalFormat = (NumberFormatInfo)NumberFormatInfo.CurrentInfo.Clone();
                        // Replace the default currency symbol with the local currency symbol.
                        LocalFormat.CurrencySymbol = "F";
                       
                        ArrayList row = new ArrayList();
                        price = Convert.ToDecimal(da.GetValue(2).ToString());
                       
                        
                      row.Add(da.GetValue(0).ToString());
                        row.Add( da.GetValue(1).ToString());
                        row.Add(price.ToString("c", NumberFormatInfo.CurrentInfo));
                        row.Add(da.GetValue(3).ToString());
                        serviceDataGridView.Rows.Add(row.ToArray());

                    }

                    if (language == "romanian")
                    {

                        // Create a CultureInfo object for French in France.
                        CultureInfo roCulture = new CultureInfo("ro-RO");
                        // Set the CurrentCulture to fr-FR.
                        Thread.CurrentThread.CurrentCulture = roCulture;
                        // Clone the NumberFormatInfo object and create
                        // a new object for the local currency of France.
                        NumberFormatInfo LocalFormatRO = (NumberFormatInfo)NumberFormatInfo.CurrentInfo.Clone();
                        // Replace the default currency symbol with the local currency symbol.
                        LocalFormatRO.CurrencySymbol = "F";
                        ArrayList row = new ArrayList();
                       decimal priceRO= Convert.ToDecimal(da.GetValue(2).ToString());

                        
                        row.Add(da.GetValue(0).ToString());
                        row.Add(da.GetValue(1).ToString());
                        row.Add(priceRO.ToString("c", NumberFormatInfo.CurrentInfo));
                        row.Add(da.GetValue(3).ToString());
                        serviceDataGridView.Rows.Add(row.ToArray());
                    }
                    
                }
            }
          
        }


        private void loadAppointmentList()
        {
            System.Data.SqlClient.SqlConnection connection = new System.Data.SqlClient.SqlConnection
               (Connection.ConString("ClinicDB"));


            connection.Open();
            //query to get all PATIENTs name 
            SqlCommand cmd = new SqlCommand("Select * FROM Appointments", connection);

            SqlDataReader da = cmd.ExecuteReader();
            
            if (da.HasRows)
            {
                
                while (da.Read())
                {


                    if (language == "english")
                    {
                        
                        CultureInfo enCulture = new CultureInfo("en-US");
                       
                        ArrayList row = new ArrayList();
                        DateTime datetime = Convert.ToDateTime(da.GetValue(1).ToString());
                        
                        DateTime time1 = Convert.ToDateTime(da.GetValue(2).ToString());
                        DateTime time2 = Convert.ToDateTime(da.GetValue(3).ToString());


                        row.Add(da.GetValue(0).ToString());
                        row.Add(datetime.ToString("D", enCulture));
                        row.Add(time1.ToString("hh:mm:ss tt", enCulture));
                        row.Add(time2.ToString("hh:mm:ss tt", enCulture));
                        row.Add(da.GetValue(4).ToString());
                        row.Add(da.GetValue(5).ToString());
                        row.Add(da.GetValue(6).ToString());
                        AppointmentsDataGridView.Rows.Add(row.ToArray());

                    }

                    if (language == "romanian")
                    {

                        
                        CultureInfo roCulture = new CultureInfo("ro-RO");

                        ArrayList row = new ArrayList();
                        DateTime date = Convert.ToDateTime(da.GetValue(1).ToString());
                        DateTime time1 = Convert.ToDateTime(da.GetValue(2).ToString());
                        DateTime time2 = Convert.ToDateTime(da.GetValue(3).ToString());


                        row.Add(da.GetValue(0).ToString());
                        row.Add(date.ToString("D", roCulture));
                        row.Add(time1.ToString("T", roCulture));
                        row.Add(time2.ToString("T", roCulture));
                        row.Add(da.GetValue(4).ToString());
                        row.Add(da.GetValue(5).ToString());
                        row.Add(da.GetValue(6).ToString());
                        AppointmentsDataGridView.Rows.Add(row.ToArray());
                    }

                }
            }

        }

        private void loadPatientList()
        {
            System.Data.SqlClient.SqlConnection connection = new System.Data.SqlClient.SqlConnection
               (Connection.ConString("ClinicDB"));


            connection.Open();
            //query to get all PATIENTs name 
            SqlCommand cmd = new SqlCommand("Select * FROM Patient", connection);

            SqlDataReader da = cmd.ExecuteReader();

            if (da.HasRows)
            {

                while (da.Read())
                {


                    if (language == "english")
                    {

                       

                        ArrayList row = new ArrayList();
                        string phone_number=String.Format("{0: ###-###-####}", double.Parse(da.GetValue(3).ToString()));
                        
                        row.Add(da.GetValue(0).ToString());
                        row.Add(da.GetValue(1).ToString());
                        row.Add(da.GetValue(2).ToString());
                        row.Add(phone_number);
                        row.Add(da.GetValue(4).ToString());
                        patientDataGridView.Rows.Add(row.ToArray());

                    }

                    if (language == "romanian")
                    {

                        ArrayList row = new ArrayList();
                        string phone_number = String.Format("{0: ###-###-####}", double.Parse(da.GetValue(3).ToString()));
                       
                        row.Add(da.GetValue(0).ToString());
                        row.Add(da.GetValue(1).ToString());
                        row.Add(da.GetValue(2).ToString());
                        row.Add(phone_number);
                        row.Add(da.GetValue(4).ToString());
                        patientDataGridView.Rows.Add(row.ToArray());
                    }

                }
            }

        }

        private void loadConsultationList()
        {
            System.Data.SqlClient.SqlConnection connection = new System.Data.SqlClient.SqlConnection
               (Connection.ConString("ClinicDB"));


            connection.Open();
            //query to get all PATIENTs name 
            SqlCommand cmd = new SqlCommand("Select * FROM Consultations", connection);

            SqlDataReader da = cmd.ExecuteReader();

            if (da.HasRows)
            {
                decimal price;
                while (da.Read())
                {


                    if (language == "english")
                    {
                        
                        CultureInfo enCulture = new CultureInfo("en-US");

                        ArrayList row = new ArrayList();
                        DateTime datetime = Convert.ToDateTime(da.GetValue(8).ToString());

                        Thread.CurrentThread.CurrentCulture = enCulture;
                        // Clone the NumberFormatInfo object and create
                        // a new object for the local currency of France.
                        NumberFormatInfo LocalFormat = (NumberFormatInfo)NumberFormatInfo.CurrentInfo.Clone();
                        // Replace the default currency symbol with the local currency symbol.
                        LocalFormat.CurrencySymbol = "F";
                        price = Convert.ToDecimal(da.GetValue(7).ToString()) / 4;

                        row.Add(da.GetValue(0).ToString());
                        row.Add(da.GetValue(1).ToString());
                        row.Add(da.GetValue(2).ToString());
                        row.Add(da.GetValue(3).ToString());
                        row.Add(da.GetValue(4).ToString());
                        row.Add(da.GetValue(5).ToString());

                        row.Add(da.GetValue(6).ToString());
                        row.Add(price.ToString("c", NumberFormatInfo.CurrentInfo));
                        row.Add(datetime.ToString("D", enCulture));


                        ConsultationDataGridView.Rows.Add(row.ToArray());

                    }

                    if (language == "romanian")
                    {


                        CultureInfo roCulture = new CultureInfo("ro-RO");

                        ArrayList row = new ArrayList();
                        DateTime date = Convert.ToDateTime(da.GetValue(8).ToString());
                        price = Convert.ToDecimal(da.GetValue(7).ToString());

                        row.Add(da.GetValue(0).ToString());
                        row.Add(da.GetValue(1).ToString());
                        row.Add(da.GetValue(2).ToString());
                        row.Add(da.GetValue(3).ToString());
                        row.Add(da.GetValue(4).ToString());
                        row.Add(da.GetValue(5).ToString());

                        row.Add(da.GetValue(6).ToString());
                        row.Add(price.ToString("c", NumberFormatInfo.CurrentInfo));
                        row.Add(date.ToString("D", roCulture));


                        ConsultationDataGridView.Rows.Add(row.ToArray());
                    }

                }
            }

        }

        private void resetMe()
        {
           
            namePatient.Text = "";
            address.Text = "";
            phone_number.Text = "";
            CNP.Text = "";

        }

       
        public void Form1_Load(object sender, EventArgs e)
        {
           
           


            // TODO: This line of code loads data into the 'doctorDataSet.Doctor' table. You can move, or remove it, as needed.
            this.doctorTableAdapter.Fill(this.doctorDataSet.Doctor);
            // TODO: This line of code loads data into the 'wardDataSet.Ward' table. You can move, or remove it, as needed.
            this.wardTableAdapter.Fill(this.wardDataSet.Ward);
            
            language = "english";
            loadServicesList();
            loadAppointmentList();
            loadConsultationList();
            loadPatientList();
            resetMe();
           
        }

        
        private void AppointmentDoctorNameComboBox_TextChanged(object sender, EventArgs e)
        {
            System.Data.SqlClient.SqlConnection connection = new System.Data.SqlClient.SqlConnection
                (Connection.ConString("ClinicDB"));
            
                connection.Open();
                SqlCommand cmd = new SqlCommand("Select Ward.name from Ward,Doctor where Doctor.name =  @name and Ward.id_ward = Doctor.id_ward ", connection);
            cmd.Parameters.AddWithValue("@name", AppointmentDoctorNameComboBox.Text);
            SqlDataReader da = cmd.ExecuteReader();
            while (da.Read())
            {
                AppointmentWardNameTextBox.Text = da.GetValue(0).ToString();
            }
            da.Close();
           
        }


        private void insertPatient_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(namePatient.Text.Trim()) || string.IsNullOrEmpty(address.Text.Trim())
              || string.IsNullOrEmpty(phone_number.Text.Trim()) || string.IsNullOrEmpty(CNP.Text.Trim()))
            {
                if (language == "romanian")
                {
                    MessageBox.Show("Te rog sa introduci toate datele pacientului ca sa poate fi salvat in baza de date", "Datele nu au fost inregistrate",
                                       MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                else
                {
                    MessageBox.Show("Please enter all patient details in order to save into database", "Data insertion failed",
                        MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                return;
            }
            
            PatientOperations operations = new PatientOperations();
            operations.InsertPatient(namePatient.Text, address.Text, phone_number.Text, CNP.Text);
            if (language == "romanian")
            {
                MessageBox.Show("Pacientul a fost inregistrat cu succes", "Inregistrarea a fost efectuata",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("Patient records successfuly saved into database", "Data insertion done",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            resetMe();
            patientDataGridView.Rows.Clear();
            loadPatientList();
            appointmentPatientNameComboBox.Items.Clear();
            loadComboBoxPatientNames();
           
           
        }

        public void patientRegistrationComplete()
        {
            nameLabel.Visible = false;
            namePatient.Visible = false;
            addressLabel.Visible = false;
            address.Visible = false;
            phone_number.Visible = false;
            phone_number.Visible = false;
            CNP_Label.Visible = false;
            CNP.Visible = false;
            insertPatient.Visible = false;
        }

        

        private void updatePatient_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(namePatient.Text.Trim()) || string.IsNullOrEmpty(address.Text.Trim())
             || string.IsNullOrEmpty(phone_number.Text.Trim()) || string.IsNullOrEmpty(CNP.Text.Trim()))
            {
                if (language == "romanian")
                {
                    MessageBox.Show("Te rog sa introduci toate datele", "Datele nu au fost inregistrate",
                   MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                else
                {
                    MessageBox.Show("Please enter all patient details in order to save into database", "Data insertion failed",
                        MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                return;
            }
            
            PatientOperations operations = new PatientOperations();
            operations.UpdatePatient(patientID,namePatient.Text, address.Text, phone_number.Text, CNP.Text);
            if (language == "romanian")
            {
                MessageBox.Show("Datele pacientului au fost modificate", "Update-ul a fost facut",
                   MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("Patient records successfuly updated into database", "Update successful",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            patientDataGridView.Rows.Clear();
            loadPatientList();
            resetMe();
            appointmentPatientNameComboBox.Items.Clear();
            loadComboBoxPatientNames();
        }

        private void patientDataGridView_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (patientDataGridView.Rows != null)
            {
                DataGridViewCellCollection cell = patientDataGridView.CurrentRow.Cells;
                patientID = Convert.ToInt32(cell[0].Value.ToString());
                namePatient.Text = cell[1].Value.ToString();
                address.Text = cell[2].Value.ToString();
                phone_number.Text = cell[3].Value.ToString();
                CNP.Text = cell[4].Value.ToString();
            }
        }

        private void deletePatient_Click(object sender, EventArgs e)
        {
            PatientOperations operations = new PatientOperations();
            operations.DeletePatient(patientID);
            if (language == "romanian")
            {
                MessageBox.Show("Datele pacientului a fost efectuata cu succes", "Stergerea a fost efetuata",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("Patient  successfuly deleted from database", "Delete successful",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            patientDataGridView.Rows.Clear();
            loadPatientList();
            resetMe();
        }

        private void insertWard_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(nameWard.Text.Trim()))
            {
                if (language == "romanian")
                {
                    MessageBox.Show("Te rog introdu datele sectiei", "Inregistrarea nu s-a efectuat",
                                       MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                else
                {
                    MessageBox.Show("Please enter the ward name in order to save it into database", "Data insertion failed",
                        MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                return;
            }
            WardOperations operations = new WardOperations();
            operations.InsertWard(nameWard.Text);
            if (language == "romanian")
            {
                MessageBox.Show("Datele sectiei au fost inregistrate cu succes", "Datele au fost inregistrate",
                   MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("Ward successfuly saved into database", "Data insertion done",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            nameWard.Text="";
            this.wardTableAdapter.Fill(this.wardDataSet.Ward);
        }

        private void wardDataGridView_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (wardDataGridView.Rows != null)
            {
                DataGridViewCellCollection cell = wardDataGridView.CurrentRow.Cells;
                wardID = Convert.ToInt32(cell[0].Value.ToString());
                nameWard.Text = cell[1].Value.ToString();
            }
        }

        private void deleteWard_Click(object sender, EventArgs e)
        {
            WardOperations operations = new WardOperations();
            operations.DeleteWard(wardID);
            if (language == "romanian")
            {
                MessageBox.Show("Datele sectiei au fost sterse cu succes", "Sectia au fost stearsa",
                                   MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("Ward  successfuly deleted from database", "Delete successful",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            this.wardTableAdapter.Fill(this.wardDataSet.Ward);
            nameWard.Text = "";
        }

        private void doctorDataGridView_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (doctorDataGridView.Rows != null)
            {
                DataGridViewCellCollection cell = doctorDataGridView.CurrentRow.Cells;
                doctorID = Convert.ToInt32(cell[0].Value.ToString());
                doctorNameTextBox.Text = cell[1].Value.ToString();
                DoctorWardNameTextBox.Text = Ward.getWardName(Convert.ToInt32(cell[2].Value.ToString()));
            }

        }

        private void insertDoctorButton_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(doctorNameTextBox.Text.Trim())|| string.IsNullOrEmpty(DoctorWardNameTextBox.Text.Trim()))
            {
                if (language == "romanian")
                {
                    MessageBox.Show("Completeaza toate datele doctorului", "Inregistrarea nu s-a efectuat",
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                else
                {
                    MessageBox.Show("Please enter doctor details in order to save it into database", "Data insertion failed",
                        MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                return;
            }
            DoctorOperations operations = new DoctorOperations();
            
            int ID = Ward.getWardID(DoctorWardNameTextBox.Text);
            operations.InsertDoctor(doctorNameTextBox.Text,ID);
            if (language == "romanian")
            {
                MessageBox.Show("Datele doctorului au fost salvate cu succes", "Datele au fost inregistrate",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("Doctor successfuly saved into database", "Data insertion done",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            doctorNameTextBox.Text = "";
            this.doctorTableAdapter.Fill(this.doctorDataSet.Doctor);
        }

        
     
        private void updateDoctorButton_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(doctorNameTextBox.Text.Trim()) || string.IsNullOrEmpty(DoctorWardNameTextBox.Text.Trim()))
            {
                if (language == "romanian")
                {

                    MessageBox.Show("Introdu toate datele medicului",
                        "Inregistrarea nu s-a efectuat", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                else
                {
                    MessageBox.Show("Please enter doctor details in order to save it into database", 
                        "Data insertion failed", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                return;
            }
            DoctorOperations operations = new DoctorOperations();
            int ID = Ward.getWardID(DoctorWardNameTextBox.Text);
            operations.UpdateDoctor(doctorID,doctorNameTextBox.Text,ID);
            if (language == "romanian")
            {
                MessageBox.Show("Datele doctorului au fost modificate cu succes",
                    "Modificarile au fost inregistrate", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("Doctor details successfuly updated into database", "Update successful",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            this.doctorTableAdapter.Fill(this.doctorDataSet.Doctor);
            doctorNameTextBox.Text = "";
        }

        private void deleteDoctorButton_Click(object sender, EventArgs e)
        {
            DoctorOperations operations = new DoctorOperations();
            operations.DeleteDoctor(doctorID);
            if (language == "romanian")
            {
                MessageBox.Show("Datele doctorului au fost sterse", "Stergerea a fost efectuata",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("Doctor  successfuly deleted from database", "Delete successful",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            this.doctorTableAdapter.Fill(this.doctorDataSet.Doctor);
            doctorNameTextBox.Text = "";
        }

      
     
      
        private void insertServicesButton_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(serviceNameTextBox.Text.Trim())|| string.IsNullOrEmpty(servicePriceTextBox.Text.Trim())
                || string.IsNullOrEmpty(serviceWardNameTextBox.Text.Trim()))
            {
                if (language == "romanian")
                {
                    MessageBox.Show("Te rog introdu toate datele serviciului", "Inregistrarea nu s-a efectuat",
                   MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                else
                {
                    MessageBox.Show("Please enter service details in order to save it into database", "Data insertion failed",
                        MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                return;
            }
            ServicesOperations operations = new ServicesOperations();
            int ID = Ward.getWardID(serviceWardNameTextBox.Text);
            try
            {
                decimal cost;
                
                    cost = Decimal.Parse(servicePriceTextBox.Text, NumberStyles.AllowDecimalPoint);
                
                operations.InsertServices(serviceNameTextBox.Text, cost , ID);
            }
            catch(FormatException exception)
            {
                if (language == "romanian")
                {
                    MessageBox.Show("Te rog introdu un numar valid!");
                }
                else
                {
                    MessageBox.Show("Please enter a valid number for price!");
                }
                serviceNameTextBox.Text = "";
                servicePriceTextBox.Text = "";
                return;
            }
            if (language == "romanian")
            {
                MessageBox.Show("Datele serviciului au fost inregistrate", "Datele au fost inregistrate",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("Service successfuly saved into database", "Data insertion done",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            serviceNameTextBox.Text = "";
            servicePriceTextBox.Text = "";
            
            serviceDataGridView.Rows.Clear();
            loadServicesList();

        }

        private void servicesUpdateButton_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(serviceNameTextBox.Text.Trim()) || string.IsNullOrEmpty(servicePriceTextBox.Text.Trim())
               || string.IsNullOrEmpty(serviceWardNameTextBox.Text.Trim()))
            {
                if (language == "romanian")
                {
                    MessageBox.Show("Te rog introdu toate datele serviciului", "Modificarea nu s-a efectuat",
                   MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                else
                {
                    MessageBox.Show("Please enter service details in order to save it into database", "Data update failed",
                        MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                return;
            }
            ServicesOperations operations = new ServicesOperations();
            int ID = Ward.getWardID(serviceWardNameTextBox.Text);
            try
            {
                decimal cost;
               
                    cost = Decimal.Parse(servicePriceTextBox.Text, NumberStyles.AllowDecimalPoint);
                
                operations.UpdateServices(serviceID, serviceNameTextBox.Text, cost, ID);

               
            }
            catch (FormatException exception)
            {
                if (language == "romanian")
                {
                    MessageBox.Show("Nu ai introdus un numar valid! ");
                }
                else
                {
                    MessageBox.Show("Please enter a valid number for price! ");
                }
                serviceNameTextBox.Text = "";
                servicePriceTextBox.Text = "";
                return;
            }
            if (language == "romanian")
            {
                MessageBox.Show("Datele serviciului au fost modificate cu succes",
                    "Modificarea s-a efectuat", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("Service details successfuly updated into database", "Update successful",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            serviceDataGridView.Rows.Clear();
            loadServicesList();
            serviceNameTextBox.Text = "";
            servicePriceTextBox.Text = "";
        }

        private void servicesDeleteButton_Click(object sender, EventArgs e)
        {
            ServicesOperations operations = new ServicesOperations();
            operations.DeleteServices(serviceID);
            if (language == "romanian")
            {
                MessageBox.Show("Datele serviciului au fost sterse cu succes", "Stergerea s-a efectuat",
                   MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("Service successfuly deleted from database", "Delete successful",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            serviceDataGridView.Rows.Clear();
            loadServicesList();
            serviceNameTextBox.Text = "";
            servicePriceTextBox.Text = "";
        }
      
        private void loadComboBoxPatientNames()
        {
            System.Data.SqlClient.SqlConnection connection = new System.Data.SqlClient.SqlConnection
               (Connection.ConString("ClinicDB"));

            
            connection.Open();
            //query to get all PATIENTs name 
            SqlCommand cmd = new SqlCommand("Select name FROM Patient", connection);
            
            SqlDataReader da = cmd.ExecuteReader();

            if (da.HasRows)
            {
                while (da.Read())
                {
                    appointmentPatientNameComboBox.Items.Add(da.GetValue(0));
                    ConsultationPatientNameComboBox.Items.Add(da.GetValue(0));

                }
            }
        }

        private void loadComboBoxDoctorNames()
        {
            System.Data.SqlClient.SqlConnection connection = new System.Data.SqlClient.SqlConnection
               (Connection.ConString("ClinicDB"));


            connection.Open();
            //query to get all PATIENTs name 
            SqlCommand cmd = new SqlCommand("Select name FROM Doctor", connection);

            SqlDataReader da = cmd.ExecuteReader();

            if (da.HasRows)
            {
                while (da.Read())
                {
                    AppointmentDoctorNameComboBox.Items.Add(da.GetValue(0));
                    ConsultationDoctorNameComboBox.Items.Add(da.GetValue(0));

                }
            }
        }


        private void InsertAppointmentButton_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(AppointmentDoctorNameComboBox.Text.Trim()) ||
              string.IsNullOrEmpty(appointmentPatientNameComboBox.Text.Trim()))
            {
                if (language == "romanian")
                {
                    MessageBox.Show("Introdu toate datele programarii", "Inregistrarea nu s-a efectuat",
                                        MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                else
                {
                    MessageBox.Show("Please fill the appointments details in order to save it into database", "Data insertion failed",
                        MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                return;
            }
            AppointmentsOperations operations = new AppointmentsOperations();
            int ID_DOCTOR = Doctor.getDoctorID(AppointmentDoctorNameComboBox.Text);
            int ID_WARD = Ward.getWardID(AppointmentWardNameTextBox.Text);
            int ID_PATIENT = Patient.getPatientID(appointmentPatientNameComboBox.Text);


            operations.InsertAppointment( ID_PATIENT, ID_DOCTOR, ID_WARD, DateAppointment.Value.Date,
                StartAppointmentdateTimePicker.Value, endAppointmentdateTimePicker.Value);
            if (language == "romanian")
            {
                MessageBox.Show("Datele programarii s-au salvat cu succes", "Datele au fost inregistrate",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("Appointment details successfuly saved into database", "Insertion done",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            AppointmentsDataGridView.Rows.Clear();
            loadAppointmentList();
        }

       

        private void DeleteAppointmentButton_Click(object sender, EventArgs e)
        {
            AppointmentsOperations operations = new AppointmentsOperations();
            operations.DeleteAppointment(appointmentID);
            if (language == "romanian")
            {
                MessageBox.Show("Datele programarii au fost sterse cu succes", "Stergere s-a efectuat",
                                    MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("Appointments successfuly deleted from database", "Delete successful",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            AppointmentsDataGridView.Rows.Clear();
            loadAppointmentList();
        }

        private void UpdateAppointmentButton_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(AppointmentDoctorNameComboBox.Text.Trim()) ||
               string.IsNullOrEmpty(appointmentPatientNameComboBox.Text.Trim()))
            {
                if (language == "romanian")
                {
                    MessageBox.Show("Introdu toate datele programarii", "Datele nu s-au modificat",
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                else
                {
                    MessageBox.Show("Please fill the appointments details in order to save it into database", "Data update failed",
                        MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                return;
            }
            AppointmentsOperations operations = new AppointmentsOperations();
            int ID_DOCTOR = Doctor.getDoctorID(AppointmentDoctorNameComboBox.Text);
            int ID_WARD = Ward.getWardID(AppointmentWardNameTextBox.Text);
            int ID_PATIENT = Patient.getPatientID(appointmentPatientNameComboBox.Text);


            operations.UpdateAppointment(appointmentID, ID_PATIENT, ID_DOCTOR, ID_WARD, DateAppointment.Value.Date,
                StartAppointmentdateTimePicker.Value, endAppointmentdateTimePicker.Value);
            if (language == "romanian")
            {
                MessageBox.Show("Datele programarii au fost modificate cu succes", "Modificarea s-a efectuat",
                                    MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("Appointment details successfuly updated into database", "Update successful",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            AppointmentsDataGridView.Rows.Clear();
            loadAppointmentList();
        }

        private void AppointmentsDataGridView_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (AppointmentsDataGridView.Rows != null)
            {
                DataGridViewCellCollection cell = AppointmentsDataGridView.CurrentRow.Cells;


                appointmentID = Convert.ToInt32(cell[0].Value.ToString());
               

            }
        }

        private void ConsultationInsertButton_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(ConsultationDoctorNameComboBox.Text.Trim()) || string.IsNullOrEmpty(ConsultationServiceNameComboBox.Text.Trim()) ||
             string.IsNullOrEmpty(ConsultationPatientNameComboBox.Text.Trim()) || string.IsNullOrEmpty(ConsultationPriceTextBox.Text.Trim()))
            {
                if (language == "romanian")
                {
                    MessageBox.Show("Introdu toate datele consultatiei", "Datele nu au fost inregistrate",
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                else
                {
                    MessageBox.Show("Please fill the consultation details in order to save it into database", "Data insertion failed",
                        MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                return;
            }
            ConsultationOperations operations = new ConsultationOperations();
            int ID_DOCTOR = Doctor.getDoctorID(ConsultationDoctorNameComboBox.Text);
            int ID_WARD = Ward.getWardID(ConsultationWardNameTextBox.Text);
            int ID_PATIENT = Patient.getPatientID(ConsultationPatientNameComboBox.Text);
            int ID_SERVICE = Services.getServiceID(ConsultationServiceNameComboBox.Text);


            operations.InsertConsultation(ID_PATIENT, ID_DOCTOR, ID_WARD, ID_SERVICE,ConsultationDate.Value.Date,
                ConsultationServiceNameComboBox.Text,Decimal.Parse(ConsultationPriceTextBox.Text),ConsultationPatientNameComboBox.Text);
            if (language == "romanian")
            {
                MessageBox.Show("Datele consultatiei au fost inregistrate", "Inregistrarea s-a efectuat",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("Consultation details successfuly saved into database", "Insertion is successful",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            ConsultationDataGridView.Rows.Clear();
            loadConsultationList();
        }

        private void ConsultationDoctorNameComboBox_TextChanged(object sender, EventArgs e)
        {
            System.Data.SqlClient.SqlConnection connection = new System.Data.SqlClient.SqlConnection
                (Connection.ConString("ClinicDB"));

            connection.Open();
            SqlCommand cmd = new SqlCommand("Select Ward.name from Ward,Doctor where Doctor.name =  @name and Ward.id_ward = Doctor.id_ward ", connection);
            cmd.Parameters.AddWithValue("@name", ConsultationDoctorNameComboBox.Text);
            SqlDataReader da = cmd.ExecuteReader();
            while (da.Read())
            {
                ConsultationWardNameTextBox.Text = da.GetValue(0).ToString();
            }
            da.Close();
        }

        private void ConsultationWardNameTextBox_TextChanged(object sender, EventArgs e)
        {
            System.Data.SqlClient.SqlConnection connection = new System.Data.SqlClient.SqlConnection
              (Connection.ConString("ClinicDB"));


            connection.Open();
            //query to get all PATIENTs name 
            SqlCommand cmd = new SqlCommand("Select Service.name FROM Service Where id_ward=@id_ward", connection);
            cmd.Parameters.AddWithValue("@id_ward", Ward.getWardID(ConsultationWardNameTextBox.Text));
            SqlDataReader da = cmd.ExecuteReader();

            if (da.HasRows)
            {
                while (da.Read())
                {
                    ConsultationServiceNameComboBox.Items.Add(da.GetValue(0));
                    

                }
            }
        }

        private void ConsultationServiceNameComboBox_TextChanged(object sender, EventArgs e)
        {
            System.Data.SqlClient.SqlConnection connection = new System.Data.SqlClient.SqlConnection
              (Connection.ConString("ClinicDB"));


            connection.Open();
            //query to get all PATIENTs name 
            SqlCommand cmd = new SqlCommand("Select price FROM Service Where name=@name", connection);
            cmd.Parameters.AddWithValue("@name", ConsultationServiceNameComboBox.Text);
            SqlDataReader da = cmd.ExecuteReader();

                while (da.Read())
                {
                    ConsultationPriceTextBox.Text=da.GetValue(0).ToString();


                }
            
        }

        private void ConsultationDataGridView_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            if (ConsultationDataGridView.Rows != null)
            {
                DataGridViewCellCollection cell = ConsultationDataGridView.CurrentRow.Cells;


                consultationID = Convert.ToInt32(cell[0].Value.ToString());
                /** 
                 ConsultationPriceTextBox.Text = decimal.Parse(System.Text.RegularExpressions.Regex.Replace(cell[7].Value.ToString(), @"[^\d.,]", "")).ToString(); 
                **/
            }
        }

        private void ConsultationUpdateButton_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(ConsultationDoctorNameComboBox.Text.Trim()) ||
              string.IsNullOrEmpty(ConsultationPatientNameComboBox.Text.Trim())|| string.IsNullOrEmpty(ConsultationServiceNameComboBox.Text.Trim()))
            {
                if (language == "romanian")
                {
                    MessageBox.Show("Introdu toate datele consultatiei", "Datele consultatiei nu s-au modificat",
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                else
                {
                    MessageBox.Show("Please fill the consultation details in order to save it into database", 
                        "Data update failed", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                return;
            }
            ConsultationOperations operations = new ConsultationOperations();
            int ID_DOCTOR = Doctor.getDoctorID(ConsultationDoctorNameComboBox.Text);
            int ID_WARD = Ward.getWardID(ConsultationWardNameTextBox.Text);
            int ID_PATIENT = Patient.getPatientID(ConsultationPatientNameComboBox.Text);
            int ID_SERVICE  = Services.getServiceID(ConsultationServiceNameComboBox.Text);


            operations.UpdateConsultation(consultationID, ID_PATIENT, ID_DOCTOR, ID_WARD, ID_SERVICE, 
                ConsultationDate.Value.Date, ConsultationServiceNameComboBox.Text, 
                Decimal.Parse(ConsultationPriceTextBox.Text), ConsultationPatientNameComboBox.Text);
            if (language == "romanian")
            {
                MessageBox.Show("Datele consultatiei s-au modificat cu succes", "Datele au fost modificate",
                   MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("Consultation details successfuly updated into database", "Update successful",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            ConsultationDataGridView.Rows.Clear();
            loadConsultationList();
        }

        private void ConsultationDeleteButton_Click(object sender, EventArgs e)
        {
            ConsultationOperations operations = new ConsultationOperations();
            operations.DeleteConsultation(consultationID);
            if (language == "romanian")
            {
                MessageBox.Show("Datele consultatiile au fost sterse", "Stergerea a fost efectuata",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("Consultation successfuly deleted from database", "Delete successful",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            ConsultationDataGridView.Rows.Clear();
            loadConsultationList();
        }






        private void comboBox1_TextChanged(object sender, EventArgs e)
        {
            switch (comboBox1.SelectedIndex)
            {
                case 0:
                    Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("en");
                    language = "english";
                   
                    break;

                case 1:
                    Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("ro");
                    language = "romanian";
                    
                    break;

            }
            this.Controls.Clear();

            InitializeComponent();
            loadServicesList();
            loadAppointmentList();
            loadConsultationList();
            loadPatientList();
            this.doctorTableAdapter.Fill(this.doctorDataSet.Doctor);
            this.wardTableAdapter.Fill(this.wardDataSet.Ward);
           
            
            
            
          
        }

        private void serviceDataGridView_CellEnter_1(object sender, DataGridViewCellEventArgs e)
        {
            if (serviceDataGridView.Rows != null)
            {
                DataGridViewCellCollection cell = serviceDataGridView.CurrentRow.Cells;
                serviceID = Convert.ToInt32(cell[0].Value.ToString());
                serviceNameTextBox.Text = cell[1].Value.ToString();
                servicePriceTextBox.Text = decimal.Parse(System.Text.RegularExpressions.Regex.Replace(cell[2].Value.ToString(), @"[^\d.,]", "")).ToString();

                serviceWardNameTextBox.Text = Ward.getWardName(Convert.ToInt32(cell[3].Value.ToString()));
            }
        }

        private void updateWard_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(nameWard.Text.Trim()))
            {
                if (language == "romanian")
                {
                    MessageBox.Show("Introdu toate datele serviciului", "Modificarea nu s-a inregistrat",
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                else
                {
                    MessageBox.Show("Please enter the ward name in order to save it into database", "Data insertion failed",
                        MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                return;
            }
            WardOperations operations = new WardOperations();
            operations.UpdateWard(wardID, nameWard.Text);
            if (language == "romanian")
            {
                MessageBox.Show("Datele au fost modificate cu succes", "Modificarea a fost efectuata",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("Ward name successfuly updated into database", "Update successful",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            this.wardTableAdapter.Fill(this.wardDataSet.Ward);
            nameWard.Text = "";
        }
    }
    }
