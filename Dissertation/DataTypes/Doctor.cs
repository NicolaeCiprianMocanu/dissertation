﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace Dissertation.DataTypes
{
    public class Doctor
    {
        public int id_doctor { get; set; }
        public int id_ward { get; set; }
        public string name { get; set; }

        public static String getDoctorName(int id)
        {
            System.Data.SqlClient.SqlConnection connection = new System.Data.SqlClient.SqlConnection
               (Connection.ConString("ClinicDB"));

            connection.Open();
            SqlCommand cmd = new SqlCommand("Select Doctor.name From Doctor Where id_doctor = @id_doctor ", connection);
            cmd.Parameters.AddWithValue("@id_doctor", id);
            SqlDataReader da2 = cmd.ExecuteReader();
            String name;
            while (da2.Read())
            {
                name = da2.GetValue(0).ToString();


                return name;
            }
            return "";
        }

        public static int getDoctorID(string name)
        {
            System.Data.SqlClient.SqlConnection connection = new System.Data.SqlClient.SqlConnection
               (Connection.ConString("ClinicDB"));

            connection.Open();
            SqlCommand cmd = new SqlCommand("SELECT id_doctor  FROM Doctor WHERE name = @name", connection);
            cmd.Parameters.AddWithValue("@name", name);
            SqlDataReader da = cmd.ExecuteReader();
            int ID;
            while (da.Read())
            {
                ID = (int)Convert.ToUInt32(da.GetValue(0));
                return ID;
            }
            return 0;
        }
    }
}
