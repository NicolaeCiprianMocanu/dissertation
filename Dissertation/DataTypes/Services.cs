﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
namespace Dissertation.DataTypes

{
    public class Services
    {
       
        public int id_service { get; set; }
        public string name { get; set; }
        public decimal price { get; set; }
        public int id_ward { get; set; }

        public static int getServiceID(string name)
        {
            System.Data.SqlClient.SqlConnection connection = new System.Data.SqlClient.SqlConnection
             (Connection.ConString("ClinicDB"));

            connection.Open();
            SqlCommand cmd = new SqlCommand("SELECT id_service  FROM Service WHERE name = @name", connection);
            cmd.Parameters.AddWithValue("@name", name);
            SqlDataReader da = cmd.ExecuteReader();
            int id;
            while (da.Read())
            {
                id = (int)Convert.ToUInt32(da.GetValue(0));
                return id;
            }
            return 0;
        }

        public static int getServicePrice(String service)
        {
            int price;
            System.Data.SqlClient.SqlConnection connection = new System.Data.SqlClient.SqlConnection
              (Connection.ConString("ClinicDB"));

            connection.Open();
            SqlCommand cmd = new SqlCommand("SELECT price  FROM Service WHERE name = @name", connection);
            cmd.Parameters.AddWithValue("@name", service);
            SqlDataReader da = cmd.ExecuteReader();

            while (da.Read())
            {
                price = (int)Convert.ToUInt32(da.GetValue(0));
                return price;
            }
            return 0;

        }


    }
}