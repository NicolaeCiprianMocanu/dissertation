﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace Dissertation.DataTypes
{
    public class Patient
    {
        public int id_patient { get; set; }
        public string name { get; set; }
        public string address { get; set; }
        public string phone_number { get; set; }
        public string cnp { get; set; }


        public static String getPatientName(int id)
        {
            System.Data.SqlClient.SqlConnection connection = new System.Data.SqlClient.SqlConnection
               (Connection.ConString("ClinicDB"));

            connection.Open();
            SqlCommand cmd = new SqlCommand("Select Patient.name From Patient Where id_patient = @id_patient ", connection);
            cmd.Parameters.AddWithValue("@id_patient", id);
            SqlDataReader da2 = cmd.ExecuteReader();
            String name;
            while (da2.Read())
            {
                name = da2.GetValue(0).ToString();


                return name;
            }
            return "";
        }

        public static int getPatientID(string name)
        {
            System.Data.SqlClient.SqlConnection connection = new System.Data.SqlClient.SqlConnection
               (Connection.ConString("ClinicDB"));

            connection.Open();
            SqlCommand cmd = new SqlCommand("SELECT id_patient  FROM Patient WHERE name = @name", connection);
            cmd.Parameters.AddWithValue("@name", name);
            SqlDataReader da = cmd.ExecuteReader();
            int ID;
            while (da.Read())
            {
                ID = (int)Convert.ToUInt32(da.GetValue(0));
                return ID;
            }
            return 0;
        }

     
    }
}
