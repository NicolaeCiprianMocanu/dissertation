﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dissertation.DataTypes
{
    public class Appointments
    {

        public int id_appointment { get; set; }
        public int id_patient { get; set; }
        public int id_doctor { get; set; }
        public int id_ward { get; set; }

        public DateTime date { get; set; }

        public DateTime start_appointment { get; set; }
        public DateTime end_appointment { get; set; }
    }
}
