﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dissertation.DataTypes
{
    public class Consultation
    {
        public int id_consultation { get; set; }
        public int id_patient { get; set; }
        public int id_doctor { get; set; }
        public int id_ward { get; set; }
        public int id_service { get; set; }
        public DateTime date { get; set; }
        public string patient_name { get; set; }
        public string service_name { get; set; }
        public decimal price { get; set; }
    }
}
