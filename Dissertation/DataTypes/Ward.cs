﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace Dissertation.DataTypes
{
    public class Ward
    {
        public int id_ward { get; set; }
        public string name { get; set; }

        public static String getWardName(int id)
        {
            System.Data.SqlClient.SqlConnection connection = new System.Data.SqlClient.SqlConnection
               (Connection.ConString("ClinicDB"));

            connection.Open();
            SqlCommand cmd = new SqlCommand("Select Ward.name From Ward Where id_ward = @id_ward ", connection);
            cmd.Parameters.AddWithValue("@id_ward", id);
            SqlDataReader da2 = cmd.ExecuteReader();
            String name;
            while (da2.Read())
            {
                name = da2.GetValue(0).ToString();


                return name;
            }
            return "";
        }

        public static int getWardID(string name)
        {
            System.Data.SqlClient.SqlConnection connection = new System.Data.SqlClient.SqlConnection
               (Connection.ConString("ClinicDB"));

            connection.Open();

            SqlCommand cmd = new SqlCommand("SELECT id_ward  FROM Ward WHERE Ward.name = @name", connection);
            cmd.Parameters.AddWithValue("@name", name);
            SqlDataReader da = cmd.ExecuteReader();
            int ID;
            while (da.Read())
            {
                ID = (int)Convert.ToUInt32(da.GetValue(0));
                return ID;
            }
            return 0;
        }

    }
}
