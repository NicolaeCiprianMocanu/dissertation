﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Dissertation.DataTypes;

namespace Dissertation.CRUD_Operations
{
    class AppointmentsOperations
    {

        public void InsertAppointment(int ID_Patient, int ID_Doctor, int ID_Ward, DateTime Date,
           DateTime Start, DateTime End)
        {
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection
                (Connection.ConString("ClinicDB")))
            {
                List<Appointments> insertAppointments = new List<Appointments>();

                insertAppointments.Add(new Appointments
                {
                    id_patient = ID_Patient,
                    id_doctor = ID_Doctor,
                    id_ward = ID_Ward,
                    date = Date,
                    start_appointment = Start,
                    end_appointment = End
                });

                connection.Execute("AppointmentRegistration @id_patient, @id_doctor,@id_ward,@date," +
                    "@start_appointment,@end_appointment",
                    insertAppointments);
                connection.Close();

            }
        }

        public void UpdateAppointment(int id, int ID_Patient, int ID_Doctor, int ID_Ward, DateTime Date,
           DateTime Start, DateTime End)
        {
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection
                (Connection.ConString("ClinicDB")))
            {
                List<Appointments> updateAppointments = new List<Appointments>();

                updateAppointments.Add(new Appointments
                {
                    id_appointment = id,
                    id_patient = ID_Patient,
                    id_doctor = ID_Doctor,
                    id_ward = ID_Ward,
                    date = Date,
                    start_appointment = Start,
                    end_appointment = End

                });

                connection.Execute("UpdateAppointment @id_appointment, @id_patient, @id_doctor,@id_ward,@date," +
                    "@start_appointment,@end_appointment",
                    updateAppointments);
                connection.Close();

            }
        }

        public void DeleteAppointment(int id)
        {
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection
                (Connection.ConString("ClinicDB")))
            {
                Appointments deleteAppoinments = new Appointments { id_appointment = id };

                connection.Execute("DeleteAppointment @id_appointment", deleteAppoinments);
                connection.Close();

            }
        }

    }
}