﻿using Dissertation.DataTypes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


using Dapper;

namespace Dissertation
{
    class PatientOperations
    {
        public void InsertPatient(string Name, string Address, string Phone_number, string Cnp)
        {
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection
                (Connection.ConString("ClinicDB")))
            {
                List<Patient> obPatient = new List<Patient>();

                obPatient.Add(new Patient
                {
                    name = Name,
                    address = Address,
                    phone_number = Phone_number,
                    cnp = Cnp
                });
              
                connection.Execute("RegisterPatient @name, @address, @phone_number, @cnp",
                    obPatient);
                connection.Close();

            }
        }

        public void UpdatePatient(int id, string Name, string Address, string Phone_number,
           string Cnp)
        {
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection
                (Connection.ConString("ClinicDB")))
            {
                List<Patient> updatePatient = new List<Patient>();

                updatePatient.Add(new Patient
                {
                    id_patient=id,
                    name = Name,
                    address = Address,
                    phone_number = Phone_number,
                    cnp=Cnp
                });

                connection.Execute("UpdatePatient @id_patient, @name, @address, @phone_number, @cnp",
                    updatePatient);
                connection.Close();

            }
        }

        public void DeletePatient(int id)
        {
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection
                (Connection.ConString("ClinicDB")))
            {
                Patient deletePatient = new Patient { id_patient = id };

                connection.Execute("DeletePatients @id_patient", deletePatient);
                connection.Close();

            }
        }

    }
}
