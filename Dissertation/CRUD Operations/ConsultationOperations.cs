﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Dissertation.DataTypes;

namespace Dissertation.CRUD_Operations
{
    class ConsultationOperations
    {

        public void InsertConsultation(int ID_Patient, int ID_Doctor, int ID_Ward, int ID_Service,DateTime Date,
           string Service,decimal Price, string Patient_name)
        {
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection
                (Connection.ConString("ClinicDB")))
            {
                List<Consultation> insertConsultation = new List<Consultation>();

                insertConsultation.Add(new Consultation
                {
                    id_patient = ID_Patient,
                    id_doctor = ID_Doctor,
                    id_ward = ID_Ward,
                    date = Date,
                    id_service=ID_Service,
                    service_name = Service,
                    patient_name = Patient_name,
                    price=Price
                });

                connection.Execute("ConsultationRegistration @id_patient, @id_doctor,@id_ward,@id_service,@date," +
                    "@service_name,@patient_name,@price", insertConsultation);
                connection.Close();

            }
        }

        public void UpdateConsultation(int id, int ID_Patient, int ID_Doctor, int ID_Ward, int ID_Service, DateTime Date,
           string Service, decimal Price, string Patient_name)
        {
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection
                (Connection.ConString("ClinicDB")))
            {
                List<Consultation> updateConsultation = new List<Consultation>();

                updateConsultation.Add(new Consultation
                {
                    id_consultation=id,
                    id_patient = ID_Patient,
                    id_doctor = ID_Doctor,
                    id_ward = ID_Ward,
                    date = Date,
                    id_service = ID_Service,
                    service_name = Service,
                    patient_name = Patient_name,
                    price = Price

                });

                connection.Execute("UpdateConsultation @id_consultation, @id_patient, @id_doctor,@id_ward,@id_service,@date," +
                    "@service_name,@patient_name,@price",
                    updateConsultation);
                connection.Close();

            }
        }

        public void DeleteConsultation(int id)
        {
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection
                (Connection.ConString("ClinicDB")))
            {
                Consultation deleteConsultation = new Consultation { id_consultation = id };

                connection.Execute("DeleteConsultation @id_consultation", deleteConsultation);
                connection.Close();

            }
        }

    }
}
