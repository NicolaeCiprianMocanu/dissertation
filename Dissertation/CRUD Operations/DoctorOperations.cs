﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Dissertation.DataTypes;

namespace Dissertation.CRUD_Operations
{
    class DoctorOperations
    {

        public void InsertDoctor(string Name, int ID_ward)
        {
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection
                (Connection.ConString("ClinicDB")))
            {
                List<Doctor> insertDoctor = new List<Doctor>();

                insertDoctor.Add(new Doctor
                {
                    name = Name,
                    id_ward = ID_ward
                });

                connection.Execute("DoctorRegister @name, @id_ward",
                    insertDoctor);
                connection.Close();

            }
        }

        public void UpdateDoctor(int id, string Name, int ID_ward)
        {
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection
                (Connection.ConString("ClinicDB")))
            {
                List<Doctor> updateDoctor = new List<Doctor>();

                updateDoctor.Add(new Doctor
                {
                    id_doctor = id,
                    name = Name,
                    id_ward = ID_ward
                });

                connection.Execute("DoctorUpdate @id_doctor,@name, @id_ward",
                    updateDoctor);
                connection.Close();

            }
        }

        public void DeleteDoctor(int id)
        {
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection
                (Connection.ConString("ClinicDB")))
            {
                Doctor deleteDoctor = new Doctor { id_doctor = id };

                connection.Execute("DeleteDoctor @id_doctor", deleteDoctor);
                connection.Close();

            }
        }

    }
}
