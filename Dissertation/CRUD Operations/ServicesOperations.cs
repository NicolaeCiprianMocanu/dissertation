﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Dissertation.DataTypes;

namespace Dissertation.CRUD_Operations
{
    class ServicesOperations
    {

        public void InsertServices(string Name,decimal Price, int ID_ward)
        {
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection
                (Connection.ConString("ClinicDB")))
            {
                List<Services> insertServices = new List<Services>();

                insertServices.Add(new Services
                {
                    name = Name,
                    price=Price,
                    id_ward = ID_ward
                });

                connection.Execute("ServiceRegister @name, @price,@id_ward",
                    insertServices);
                connection.Close();

            }
        }

        public void UpdateServices(int id, string Name, decimal Price,int ID_ward)
        {
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection
                (Connection.ConString("ClinicDB")))
            {
                List<Services> updateServices = new List<Services>();

                updateServices.Add(new Services
                {
                    id_service = id,
                    name = Name,
                    price=Price,
                    id_ward = ID_ward

                });

                connection.Execute("ServiceUpdate @id_service,@name, @price,@id_ward",
                    updateServices);
                connection.Close();

            }
        }

        public void DeleteServices(int id)
        {
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection
                (Connection.ConString("ClinicDB")))
            {
                Services deleteServices = new Services { id_service = id };

                connection.Execute("DeleteServices @id_service", deleteServices);
                connection.Close();

            }
        }

       
    }
}
