﻿using Dissertation.DataTypes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


using Dapper;

namespace Dissertation.CRUD_Operations
{
    class WardOperations
    {

        public void InsertWard(string Name)
        {
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection
                (Connection.ConString("ClinicDB")))
            {
                List<Ward> insertWard = new List<Ward>();

                insertWard.Add(new Ward
                {
                    name = Name
                    
                });

                connection.Execute("RegisterWard @name",insertWard);
                connection.Close();

            }
        }

        public void UpdateWard(int id, string Name)
        {
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection
                (Connection.ConString("ClinicDB")))
            {
                List<Ward> updateWard = new List<Ward>();

                updateWard.Add(new Ward
                {
                    id_ward = id,
                    name = Name
                });

                connection.Execute("UpdateWard @id_ward,@name",
                    updateWard);
                connection.Close();

            }
        }

        public void DeleteWard(int id)
        {
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection
                (Connection.ConString("ClinicDB")))
            {
                Ward deleteWard = new Ward { id_ward = id };

                connection.Execute("DeleteWard @id_ward", deleteWard);
                connection.Close();

            }
        }
/**
        public int getWardID(String Name)
        {
            using (IDbConnection connection = new System.Data.SqlClient.SqlConnection
                (Connection.ConString("ClinicDB")))
            {
                Ward ward= new Ward { name = Name };

                int ID = connection.Execute("GetWardID @name", ward);
                connection.Close();
                return ID;
            }
        }
*/
    }
}
