﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Dissertation;
using System;
//using Dissertation.CRUD Operations;
using Dissertation.DataTypes;
using System.Threading;
using System.Windows.Forms;
using System.Globalization;

namespace DissertationTest
{
    [TestClass]
    public class UnitTest
    {
        [TestMethod]
        public void TestAmericanDate()
        {

            CultureInfo enCulture = new CultureInfo("en-US");

            DateTime date = Convert.ToDateTime("10/07/2020");
            string actual = date.ToString("D", enCulture);

            string expected = "Friday, July 10, 2020";

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestRomanianDate()
        {

            CultureInfo roCulture = new CultureInfo("ro-RO");

            DateTime date = Convert.ToDateTime("10/07/2020");
            string actual = date.ToString("D", roCulture);

            string expected = "miercuri, 7 octombrie 2020";

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestAmericanTime()
        {

            CultureInfo enCulture = new CultureInfo("en-US");

            DateTime time1 = Convert.ToDateTime("20:30:00");
            string actual = time1.ToString("hh:mm:ss tt", enCulture);
           
            string expected = "08:30:00 PM";

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void TestRomanianTime()
        {

            CultureInfo roCulture = new CultureInfo("ro-RO");

            DateTime time1 = Convert.ToDateTime("08:30:00 PM");
            string actual = time1.ToString("T", roCulture);

            string expected = "20:30:00";

            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void TestRomanianNumberFormatAndCurrency()
        {

            CultureInfo roCulture = new CultureInfo("ro-RO");

            Thread.CurrentThread.CurrentCulture = roCulture;


            NumberFormatInfo LocalFormat = (NumberFormatInfo)NumberFormatInfo.CurrentInfo.Clone();

            LocalFormat.CurrencySymbol = "F";
            decimal price = 1000;
            string actual = price.ToString("c", NumberFormatInfo.CurrentInfo);

            string expected = "1.000,00 lei";

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public  void TestAmericanNumberFormatAndCurrency()
        {

            CultureInfo enCulture = new CultureInfo("en-US");
            
            Thread.CurrentThread.CurrentCulture = enCulture;
           
          
            NumberFormatInfo LocalFormat = (NumberFormatInfo)NumberFormatInfo.CurrentInfo.Clone();
            
            LocalFormat.CurrencySymbol = "F";
            decimal price = 1000;
            string actual = price.ToString("c", NumberFormatInfo.CurrentInfo);

            string expected = "$1,000.00";
         
            Assert.AreEqual(expected ,actual);
        }
       
        }
}

